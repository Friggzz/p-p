<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210903122247 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE association (id INT AUTO_INCREMENT NOT NULL, location_id_id INT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, phone VARCHAR(25) NOT NULL, mail VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_FD8521CC918DB72 (location_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_link (association_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_190E62F0EFB9C8A5 (association_id), INDEX IDX_190E62F0ADA40271 (link_id), PRIMARY KEY(association_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE breed (id INT AUTO_INCREMENT NOT NULL, species_id INT NOT NULL, label VARCHAR(25) NOT NULL, INDEX IDX_F8AF884FB2A1D860 (species_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE care_group (id INT AUTO_INCREMENT NOT NULL, link_id INT NOT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_F383BEB7ADA40271 (link_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE color (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE details (id INT AUTO_INCREMENT NOT NULL, tattoo_id INT NOT NULL, chip_id INT NOT NULL, species_id INT NOT NULL, breed_id INT NOT NULL, fur_id INT NOT NULL, gender_id INT NOT NULL, INDEX IDX_72260B8A3EB48656 (tattoo_id), INDEX IDX_72260B8AA588ADB3 (chip_id), INDEX IDX_72260B8AB2A1D860 (species_id), INDEX IDX_72260B8AA8B4A30F (breed_id), INDEX IDX_72260B8A66B566E (fur_id), INDEX IDX_72260B8A708A0E0 (gender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE details_color (details_id INT NOT NULL, color_id INT NOT NULL, INDEX IDX_F6B9EE06BB1A0722 (details_id), INDEX IDX_F6B9EE067ADA1FB5 (color_id), PRIMARY KEY(details_id, color_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fur (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE gender (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE identification (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, annonces_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_E01FBE6A4C2885D7 (annonces_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link (id INT AUTO_INCREMENT NOT NULL, linktype_id INT NOT NULL, url VARCHAR(255) NOT NULL, INDEX IDX_36AC99F15DBB2F9F (linktype_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link_type (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, pays VARCHAR(50) NOT NULL, departement VARCHAR(50) NOT NULL, city VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts (id INT AUTO_INCREMENT NOT NULL, users_id INT NOT NULL, details_id INT NOT NULL, category_id INT NOT NULL, location_id INT NOT NULL, date DATE NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_885DBAFA67B3B43D (users_id), UNIQUE INDEX UNIQ_885DBAFABB1A0722 (details_id), INDEX IDX_885DBAFA12469DE2 (category_id), INDEX IDX_885DBAFA64D218E (location_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schools (id INT AUTO_INCREMENT NOT NULL, location_id_id INT NOT NULL, name VARCHAR(100) NOT NULL, address VARCHAR(255) NOT NULL, phone VARCHAR(25) NOT NULL, mail VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_47443BD5918DB72 (location_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schools_link (schools_id INT NOT NULL, link_id INT NOT NULL, INDEX IDX_C51D9ABDA000581D (schools_id), INDEX IDX_C51D9ABDADA40271 (link_id), PRIMARY KEY(schools_id, link_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE species (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(100) NOT NULL, firstname VARCHAR(100) NOT NULL, phone VARCHAR(25) DEFAULT NULL, mail VARCHAR(100) DEFAULT NULL, nickname VARCHAR(100) NOT NULL, role INT NOT NULL, pass VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE association ADD CONSTRAINT FK_FD8521CC918DB72 FOREIGN KEY (location_id_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE association_link ADD CONSTRAINT FK_190E62F0EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE association_link ADD CONSTRAINT FK_190E62F0ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE breed ADD CONSTRAINT FK_F8AF884FB2A1D860 FOREIGN KEY (species_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE care_group ADD CONSTRAINT FK_F383BEB7ADA40271 FOREIGN KEY (link_id) REFERENCES link (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8A3EB48656 FOREIGN KEY (tattoo_id) REFERENCES identification (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8AA588ADB3 FOREIGN KEY (chip_id) REFERENCES identification (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8AB2A1D860 FOREIGN KEY (species_id) REFERENCES species (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8AA8B4A30F FOREIGN KEY (breed_id) REFERENCES breed (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8A66B566E FOREIGN KEY (fur_id) REFERENCES fur (id)');
        $this->addSql('ALTER TABLE details ADD CONSTRAINT FK_72260B8A708A0E0 FOREIGN KEY (gender_id) REFERENCES gender (id)');
        $this->addSql('ALTER TABLE details_color ADD CONSTRAINT FK_F6B9EE06BB1A0722 FOREIGN KEY (details_id) REFERENCES details (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE details_color ADD CONSTRAINT FK_F6B9EE067ADA1FB5 FOREIGN KEY (color_id) REFERENCES color (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A4C2885D7 FOREIGN KEY (annonces_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F15DBB2F9F FOREIGN KEY (linktype_id) REFERENCES link_type (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFA67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFABB1A0722 FOREIGN KEY (details_id) REFERENCES details (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFA12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFA64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE schools ADD CONSTRAINT FK_47443BD5918DB72 FOREIGN KEY (location_id_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE schools_link ADD CONSTRAINT FK_C51D9ABDA000581D FOREIGN KEY (schools_id) REFERENCES schools (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE schools_link ADD CONSTRAINT FK_C51D9ABDADA40271 FOREIGN KEY (link_id) REFERENCES link (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE association_link DROP FOREIGN KEY FK_190E62F0EFB9C8A5');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8AA8B4A30F');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFA12469DE2');
        $this->addSql('ALTER TABLE details_color DROP FOREIGN KEY FK_F6B9EE067ADA1FB5');
        $this->addSql('ALTER TABLE details_color DROP FOREIGN KEY FK_F6B9EE06BB1A0722');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFABB1A0722');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8A66B566E');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8A708A0E0');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8A3EB48656');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8AA588ADB3');
        $this->addSql('ALTER TABLE association_link DROP FOREIGN KEY FK_190E62F0ADA40271');
        $this->addSql('ALTER TABLE care_group DROP FOREIGN KEY FK_F383BEB7ADA40271');
        $this->addSql('ALTER TABLE schools_link DROP FOREIGN KEY FK_C51D9ABDADA40271');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F15DBB2F9F');
        $this->addSql('ALTER TABLE association DROP FOREIGN KEY FK_FD8521CC918DB72');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFA64D218E');
        $this->addSql('ALTER TABLE schools DROP FOREIGN KEY FK_47443BD5918DB72');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A4C2885D7');
        $this->addSql('ALTER TABLE schools_link DROP FOREIGN KEY FK_C51D9ABDA000581D');
        $this->addSql('ALTER TABLE breed DROP FOREIGN KEY FK_F8AF884FB2A1D860');
        $this->addSql('ALTER TABLE details DROP FOREIGN KEY FK_72260B8AB2A1D860');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFA67B3B43D');
        $this->addSql('DROP TABLE association');
        $this->addSql('DROP TABLE association_link');
        $this->addSql('DROP TABLE breed');
        $this->addSql('DROP TABLE care_group');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE color');
        $this->addSql('DROP TABLE details');
        $this->addSql('DROP TABLE details_color');
        $this->addSql('DROP TABLE fur');
        $this->addSql('DROP TABLE gender');
        $this->addSql('DROP TABLE identification');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE link');
        $this->addSql('DROP TABLE link_type');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE posts');
        $this->addSql('DROP TABLE schools');
        $this->addSql('DROP TABLE schools_link');
        $this->addSql('DROP TABLE species');
        $this->addSql('DROP TABLE users');
    }
}
