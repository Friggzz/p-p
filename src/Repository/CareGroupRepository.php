<?php

namespace App\Repository;

use App\Entity\CareGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CareGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method CareGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method CareGroup[]    findAll()
 * @method CareGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CareGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CareGroup::class);
    }

    // /**
    //  * @return CareGroup[] Returns an array of CareGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CareGroup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
