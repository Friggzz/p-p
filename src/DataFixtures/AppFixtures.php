<?php

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\Breed;
use App\Entity\CareGroup;
use App\Entity\Category;
use App\Entity\Color;
use App\Entity\Details;
use App\Entity\Fur;
use App\Entity\Gender;
use App\Entity\Identification;
use App\Entity\Link;
use App\Entity\LinkType;
use App\Entity\Location;
use App\Entity\Posts;
use App\Entity\Schools;
use App\Entity\Species;
use App\Entity\Users;
use Faker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // // $manager->persist($product);

        // $faker = Faker\Factory::create('fr_FR');
        
        // CATEGORY

        $categories = ['Perdu', 'Volé', 'Trouvé', 'Apperçu'];

        foreach ($categories as $key => $category){
            $c = new Category();
            $c->setLabel($category);

            $this->setReference('Cat' . $key, $c );
            $manager->persist($c); // persistance de l'objet pour le flush en base
        }

        // FUR

        $furs = ['Long', 'Court'];

        foreach ($furs as $key => $fur){
            $f = new Fur();
            $f->setLabel($fur);

            $this->setReference('Fur' . $key, $f );
            $manager->persist($f);
        }

        // SPECIES

        $speciesV = ['Chat','Cheval','Chien', 'Chèvre', 'Lapin', 'Oiseau', 'Tortue', 'Autre'];

        foreach ($speciesV as $key => $species){
            $s = new Species();
            $s->setLabel($species);

            $this->setReference('S' . $key, $s );
            $manager->persist($s);

            // Création du type Inconnu dans chaque Espèce, commune à toutes
            $b = new Breed();
            $b->setLabel('Inconnu')
            ->setSpecies($this->getReference('S' . $key) );
            $this->setReference('BI' . $key, $b ); // référencement du type Inconnu

            $manager->persist($b);
        }

        // // BREED

        $breeds = ['Abyssain' => 0, 'Bengal'=> 0,  'Bombay' => 0, 'Bleu de Russie' => 0, 'Chat de goutière' => 0, 'Chartreux' => 0, 'Maine Coon' => 0, 'Persan' => 0, 'Siamois' => 0, 'Sphynx' => 0, 'Berger Allemand' => 3, 'Berger des Pyrénées' => 3, 'Border Collie' => 3, 'Danois' => 3, 'Dobermann' => 3, 'Husky' => 3, 'Labrador' => 3, 'Lévrier Afghan' => 3, 'Rottweiler' => 3, 'Shiba' => 3];

        $c = 0;

        foreach ($breeds as $key => $value){
            $b = new Breed();
            $b->setLabel($key)
            ->setSpecies($this->getReference('S' . $value));

            $this->setReference('B' . $c, $b );
            $c++;
            $manager->persist($b);
        }

        // GENDER

        $genders = ['Femelle', 'Mâle', 'Ne sais pas'];

        foreach ($genders as $key => $gender){
            $g = new Gender();
            $g->setLabel($gender);

            $this->setReference('G' . $key, $g );
            $manager->persist($g);
        }

        // IDENTIFICATION

        $identifications = ['Oui', 'Non', 'Ne sais pas'];

        foreach ($identifications as $key => $identification){
            $i = new Identification();
            $i->setLabel($identification);

            $this->setReference('I' . $key, $i );
            $manager->persist($i);
        }

        // COLOR

        $colors = ['Noir', 'Blanc', 'Gris', 'Marron', 'Roux', 'Isabelle', 'Tigré', 'Feu', 'Fauve'];

        foreach ($colors as $key => $color){
            $c = new Color();
            $c->setLabel($color);

            $this->setReference('Col' . $key, $c );
            $manager->persist($c);
        }

        // // LINKTYPE

        // $linktypes = ['Site Web', 'Facebook', 'Instagram', 'Twitter', 'LinkedIn'];

        // foreach ($linktypes as $key => $linktype){
        //     $l = new LinkType();
        //     $l->setLabel($linktype);

        //     $this->setReference('LT' . $key, $l );
        //     $manager->persist($l);
        // }

        // LOCATION

        $locations = ['Bourg-en-Bresse' => ['France', '01 - Ain'],'Laon' => ['France', '02 - Aisne'], 'Moulins' => ['France', '03 - Allier'], 'Digne-Les-Bains' => ['France', '04 - Alpes-de-Haute-Provence'], 'Gap' => ['France', '05 - Hautes-Alpes'], 'Perpignan' => ['France', 'Pyrénées-Orientales'], 'Lyon' => ['France', '69 - Rhône-Alpes'], 'Paris' => ['France', 'Ile-de-France'], 'Bruxelles' => ['Belgique', ' - '], 'Uetendorf' => ['Suisse', 'Berne'], 'Genève' => ['Suisse', 'Région lémanique'], 'Montréal' => ['Canada', 'Québec'] ];

        foreach ($locations as $key => $value){
            $l = new Location();
            $l->setPays($value[0])
            ->setCity($key)
            ->setDepartement($value[1]);

            $this->setReference('Loc' . $key, $l );
            $manager->persist($l);
        }

        // USERS

        $user = new Users();
        $user->setLastname('LeGaulois')->setFirstname('Provençal')
            ->setMail('perceval.gallois@kaa.com')
            ->setPhone('1234567890')
            ->setNickname('Sloubi')
            ->setPass($this->encoder->hashPassword($user, 'demo'))
            ->setRole(1);
        $this->setReference('U' . 0, $user);
        $manager->persist($user);

        $user2 = new Users();
        $user2->setLastname('Jack')->setFirstname('Céparou')
            ->setMail('blackpearl@caraibes.com')
            ->setPhone('0123456789')
            ->setNickname('Capitaine')
            ->setPass($this->encoder->hashPassword($user2, 'demo'));
        $this->setReference('U' . 1, $user2);
        $manager->persist($user2);

        // DETAILS

        for($i = 0; $i < 30; $i++){
            $details = new Details();
            $details->setSpecies($this->getReference('S' . rand(0, count($speciesV) - 1 )))
                ->setBreed($this->getReference('B' . rand(0, count($breeds) - 1 )))
                ->setChip($this->getReference('I' . rand(0, count($identifications) - 1 )))
                ->setTattoo($this->getReference('I' . rand(0, count($identifications) - 1 )))
                ->setGender($this->getReference('G' . rand(0, count($genders) - 1 )))
                ->setFur($this->getReference('Fur' . rand(0, count($furs) - 1 )));

            $this->setReference('D' . $i, $details);
            $manager->persist($details);
        }

        // // POSTS

        // for( $i=0; $i<30; $i++ ) {
        //     $post = new Posts();
        //     $post->setCategory($this->getReference('Cat' . rand(0, count($categories) - 1 )))
        //         ->setDate(new \DateTime($faker->date('Y-m-d', 'now') ))
        //         ->setDetails($this->getReference('D' . rand(0, count($furs) - 1 )))
        //         ->setLocation($this->getReference('Loc' . rand(0, count($locations) - 1 )))
        //         ->setUsers($this->getReference('U' . rand(0, 1)))
        //         ->setDescription($faker->text);
            
        //     $this->setReference('P' . $i, $post);
        //     $manager->persist($post);
        // }

        // // ASSOCIATIONS

        // for($i = 0; $i < 20; $i++){
        //     $a = new Association();
        //     $a->setName($faker->name)
        //         ->setAddress($faker->address)
        //         ->setPhone($faker->phoneNumber)
        //         ->setMail($faker->email)
        //         ->setLocationId($this->getReference('Loc' . rand(0, count($locations) - 1 )))
        //         ->setDescription($faker->text);

        //     $this->setReference('A' . $i, $a);
        //     $manager->persist($a);
        // }

        // // SCHOOL & LINKS
        // for($i = 0; $i <30; $i++){
        //     $l= new Link();
        //     $l->setLinktype($this->getReference('LT' . rand(0, count($linktypes) - 1 )))
        //         ->setUrl($faker->url);

        //     $this->setReference('Lk' . $i, $l);
        //     $manager->persist($l);
        // // }

        // // SCHOOL

        // // for($i = 0; $i < 20; $i++){
        //     $s = new Schools();
        //     $s->setName($faker->name)
        //         ->setAddress($faker->address)
        //         ->setPhone($faker->phoneNumber)
        //         ->setMail($faker->email)
        //         ->setLocationId($this->getReference('Loc' . rand(0, count($locations) - 1 )))
        //         ->setDescription($faker->text)
        //         ->getLinksId() -> add($l);
                
        //     $this->setReference('S' . $i, $s);
        //     $manager->persist($s);

            
        // }

        
        // // SCHOOL LINKS

        // for($i=0; $i<30; $i++){
        //     $sl = new Schools_link();
        //     $sl->setSchools_id($this->getReference('S' . rand(0, 9)))
        //         ->setLink_id($this->getReference('Lk' . rand(0, 9)));

        // $this->setReference('SL' . $i, $sl);
        // $manager->persist($sl);
        // }

        // // ASSO LINKS

        // for($i=0; $i<30; $i++){
        //     $al = new Association_link();
        //     $al->setAssociation_id($this->getReference('A' . rand(0, 9)))
        //         ->setLink_id($this->getReference('Lk' . rand(0, 9)));

        // $this->setReference('AL' . $i, $al);
        // $manager->persist($al);
        // }
        

        $manager->flush();
    }
}
