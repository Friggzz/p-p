<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class HomeController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="home")
     *
     * @return Response
     */   
    public function bienvenue()
    {
        return $this->render("home/home.html.twig");
    }

   

}