<?php
namespace App\Controller;

use App\Entity\Posts;
use App\Entity\Images;
use App\Form\EditProfileType;
use App\Repository\PostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// /**
//  * Class UserController
//  * @package App\Controller
//  * @Route ("/users")
//  */
class UserController extends AbstractController
{
     /**
     * @Route("/users", name="users")
     */
    public function index() {
    return $this->render("users/profile.html.twig", ['controller_name' => 'UserContoller']);
    }

    /**
     * @Route("/users/editprofile", name="users_edit_profile")
     */
    public function editProfile(Request $request) 
    {
        $user = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil mis à jour');
            return $this->redirectToRoute('users');
        }

        return $this->render('users/editprofile.html.twig', ['form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/postedit/{id}", name="upornew", methods={"GET", "POST"})
     * 
     * @param Request $request
     */
    public function createOrUpdate(int $id = -1, PostsRepository $postsRepository, Request $request)
    {
        if($id == -1)
            $posts = new Posts();    
        else
            $posts = $postsRepository->find($id);

        $form = $this->createForm(PostsRepository::class,$posts);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // On récupère les images transmises
            $images = $form->get('images')->getData();
            
            // On boucle sur les images
            foreach($images as $image){
                // On génère un nouveau nom de fichier
                $fichier = md5(uniqid()).'.'.$image->guessExtension();
                
                // On copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier
                );
                
                // On crée l'image dans la base de données
                $img = new Images();
                $img->setName($fichier);
                $posts->addImage($img);
            }

            $manager = $this->getDoctrine()->getManager(); // appel du manager doctrine
            $manager->persist($posts);
            $manager->flush();
            return $this->redirectToRoute('home');

        }    
    }

    /**
     * @Route("/del/{id}", name="delpost", methods={"POST"})
     */
    public function deletePost(Posts $posts, Request $request)
    {
        if($this->isCsrfTokenValid('delete' . $posts->getID(),
        $request->get('_token'))){
            $dp = $this->getDoctrine()->getManager();
            
            $images = $posts->getImages();
            
            dump($images);
            
            
            // $dp->remove($posts);
            $dp->flush();

            $this->addFlash('success', "l'annonce a bien été supprimée !!");

            return $this->redirectToRoute('home');
        }
    }


}