<?php

namespace App\Form;

use App\Entity\Details;
use App\Entity\Users;
use App\Entity\Category;
use App\Entity\Location;
use App\Entity\Posts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;


class PostsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('date')
            ->add('description')
            ->add('category', EntityType::class, [
                'mapped' => false,
                'class' => Category::class,
                'choice_label' => 'label',
                'placeholder' => 'Catégorie',
                'label' => 'Catégorie'
                ])
                ->add('location', EntityType::class, [
                    'mapped' => false,
                    'class' => Location::class,
                    'choice_label' => 'city',
                    'placeholder' => 'Choissisez une ville',
                    'label' => 'Ville'   
                ])

                ->add('details', DetailsType::class, [
                    'data_class' => Details::class,
                ])
            // On ajoute le champ "images" dans le formulaire
            // Il n'est pas lié à la base de données (mapped à false)
            ->add('images', FileType::class,[
                'label' => false,
                'multiple' => true,
                'mapped' => true,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Posts::class,
        ]);
    }

}
