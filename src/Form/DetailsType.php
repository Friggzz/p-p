<?php

namespace App\Form;

use App\Entity\Species;
use App\Entity\Identification;
use App\Entity\Gender;
use App\Entity\Color;
use App\Entity\Fur;
use App\Entity\Breed;
use App\Entity\Details;
use App\Repository\SpeciesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tattoo', EntityType::class, [
                'mapped' => true,
                'class' => Identification::class,
                'choice_label' => 'label',
                'label' => 'Tatoué(e)'
            ])
            ->add('chip', EntityType::class, [
                'mapped' => true,
                'class' => Identification::class,
                'choice_label' => 'label',
                'label' => 'Pucé(e)'

            ])
            ->add('species', EntityType::class, [
                'mapped' => true,
                'class' => Species::class,
                'choice_label' => 'label',
                'placeholder' => 'Espèce',
                'label' => 'Espèce'
            ])
            ->add('breed', EntityType::class, [
                'mapped' => true,
                'class' => Breed::class,
                'choice_label' => 'label',
                'placeholder' => 'Race',
                'label' => 'Race'
            ])
            ->add('fur', EntityType::class, [
            'mapped' => true,
            'class' => Fur::class,
            'choice_label' => 'label',
            'placeholder' => 'Longueur',
            'label' => 'Poils'
            ])
            ->add('color', EntityType::class, [
                'mapped' => false,
                'class' => Color::class,
                'choice_label' => 'label',
                'placeholder' => 'Couleur',
                'label' => 'Couleur 1'
            ])
            // ->add('color2', EntityType::class, [
            //     'mapped' => true,
            //     'class' => Color::class,
            //     'choice_label' => 'label',
            //     'placeholder' => 'Couleur',
            //     'label' => 'Couleur 2'
            // ])
            ->add('gender', EntityType::class, [
                'mapped' => true,
                'class' => Gender::class,
                'choice_label' => 'label',
                'placeholder' => 'Genre',
                'label' => 'Genre'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Details::class,
        ]);
    }
}
