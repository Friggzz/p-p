<?php

namespace App\Entity;

use App\Repository\DetailsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=DetailsRepository::class)
 */
class Details
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Identification::class, inversedBy="details_tattoo")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tattoo;

    /**
     * @ORM\ManyToOne(targetEntity=Identification::class, inversedBy="details_chip")
     * @ORM\JoinColumn(nullable=false)
     */
    private $chip;

    /**
     * @ORM\OneToOne(targetEntity=Posts::class, mappedBy="details", cascade={"persist", "remove"})
     */
    private $posts;

    /**
     * @ORM\ManyToOne(targetEntity=Species::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $species;

    /**
     * @ORM\ManyToOne(targetEntity=Breed::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $breed;

    /**
     * @ORM\ManyToOne(targetEntity=Fur::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $fur;

    /**
     * @ORM\ManyToMany(targetEntity=Color::class)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity=Gender::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $gender;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTattoo(): ?Identification
    {
        return $this->tattoo;
    }

    public function setTattoo(?Identification $tattoo): self
    {
        $this->tattoo = $tattoo;

        return $this;
    }

    public function getChip(): ?Identification
    {
        return $this->chip;
    }

    public function setChip(?Identification $chip): self
    {
        $this->chip = $chip;

        return $this;
    }

    public function getPosts(): ?Posts
    {
        return $this->posts;
    }

    public function setPosts(Posts $posts): self
    {
        // set the owning side of the relation if necessary
        if ($posts->getDetails() !== $this) {
            $posts->setDetails($this);
        }

        $this->posts = $posts;

        return $this;
    }

    public function getSpecies(): ?Species
    {
        return $this->species;
    }

    public function setSpecies(?Species $species): self
    {
        $this->species = $species;

        return $this;
    }

    public function getBreed(): ?Breed
    {
        return $this->breed;
    }

    public function setBreed(?Breed $breed): self
    {
        $this->breed = $breed;

        return $this;
    }

    public function getFur(): ?Fur
    {
        return $this->fur;
    }

    public function setFur(?Fur $fur): self
    {
        $this->fur = $fur;

        return $this;
    }

    public function getColor(): ?Color
    {
        return $this->color;
    }

    public function setColor(?Color $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }
}
