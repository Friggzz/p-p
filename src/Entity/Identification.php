<?php

namespace App\Entity;

use App\Repository\IdentificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IdentificationRepository::class)
 */
class Identification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity=Details::class, mappedBy="tattoo")
     */
    private $details_tattoo;

    /**
     * @ORM\OneToMany(targetEntity=Details::class, mappedBy="chip")
     */
    private $details_chip;

    public function __construct()
    {
        $this->details_tattoo = new ArrayCollection();
        $this->chip = new ArrayCollection();
        $this->details_chip = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Details[]
     */
    public function getDetailsTatto(): Collection
    {
        return $this->details_tattoo;
    }

    public function addDetailTattoo(Details $detailsTattoo): self
    {
        if (!$this->detailsTattoo->contains($detailsTattoo)) {
            $this->detailsTattoo[] = $detailsTattoo;
            $detailsTattoo->setTattoo($this);
        }

        return $this;
    }

    public function removeDetailsTattoo(Details $detailsTattoo): self
    {
        if ($this->details_tattoo->removeElement($detailsTattoo)) {
            // set the owning side to null (unless already changed)
            if ($detailsTattoo->getTattoo() === $this) {
                $detailsTattoo->setTattoo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Details[]
     */
    public function getDetailsChip(): Collection
    {
        return $this->details_chip;
    }

    public function addDetailsChip(Details $detailsChip): self
    {
        if (!$this->details_chip->contains($detailsChip)) {
            $this->details_chip[] = $detailsChip;
            $detailsChip->setChip($this);
        }

        return $this;
    }

    public function removeDetailsChip(Details $detailsChip): self
    {
        if ($this->details_chip->removeElement($detailsChip)) {
            // set the owning side to null (unless already changed)
            if ($detailsChip->getChip() === $this) {
                $detailsChip->setChip(null);
            }
        }

        return $this;
    }
    public function __toString() : string {
        return $this->identification;
    }
}
